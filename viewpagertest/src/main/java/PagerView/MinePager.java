package PagerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.viewpagertest.DetailsActivity;
import com.example.viewpagertest.R;

import Tools.GlideCircleTransform;
import Tools.RadioButtonRestoreUtils;

/**
 * Created by 陈湘 on 2018/7/11.
 */

public class MinePager extends BasePager{
    public MinePager(Context context) {
        super(context);
    }
    private ImageView me_iv;
    TextView me_xm,me_qm;
    private RadioGroup rg;


    @Override
    public View initView() {
        LinearLayout mepage;
        View view = View.inflate(context, R.layout.me,null);
        mepage = (LinearLayout) view.findViewById(R.id.me_page);
        return view;
    }
    @Override
    public void iniData() {
        initView();
        me_iv= (ImageView) mRootView.findViewById(R.id.me_iv);
        Glide.with(context)
                .load(R.drawable.header_img)
                .transform(new GlideCircleTransform(context))
                .into(me_iv);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.bt_details:
                        DetailsDialog();
                        break;
                    case R.id.bt_version:
                        VersonDialog();
                        break;
                    case R.id.bt_developer:
                        DeveloperDialog();
                        break;

                }
                RadioButtonRestoreUtils.restoredRadioButton(checkedId,group,this,context);
            }
        });


    }

    private void DetailsDialog() {
        Intent intent=new Intent(context, DetailsActivity.class);
        //intent.setClass(context,DetailsActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //AppCompatActivity mActivity=(AppCompatActivity)context;   //强制转化后才能使用forresult
        startActivityForResult(intent,1);

        //在mainActivity里写Onresult函数
    }

    //在这里写有关forresult的方法，在其他非Activity的地方不能写。
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    byte buff[] = data.getByteArrayExtra("data_pic");
                    Bitmap bitmap = BitmapFactory.decodeByteArray(buff, 0, buff.length);
                    ImageView imageView = (ImageView) findViewById(R.id.me_iv);
                    imageView.setImageBitmap(bitmap);
                }
                break;
        }
    }

    private void VersonDialog() {
        TextView msg = new TextView(context);
        msg.setText("\n"+"V1.0\n" +"Copyright © 2018 Group4\n" +  "All Right Reserved\n");
        msg.setPadding(30, 10, 10, 5);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(18);
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("版本号：")
                .setView(msg)
                .setPositiveButton("确认",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                } )
                .create();
        dialog.show();
    }

    private void DeveloperDialog() {
        TextView msg = new TextView(context);
        msg.setText("\n"+"杨铮伟\n" +"陈湘\n" +  "侯忠博\n" +"黄延\n");
        msg.setPadding(30, 10, 10, 5);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(18);
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Designed By: Group4")
                .setView(msg)
                .setPositiveButton("确认",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                } )
                .create();
        dialog.show();
    }

}
