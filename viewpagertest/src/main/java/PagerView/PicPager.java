package PagerView;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

/**
 * Created by 陈湘 on 2018/7/11.
 */

public class PicPager extends BasePager {
    public PicPager(Context context) {
        super(context);
    }

    @Override
    public void iniData() {
        TextView tv = new TextView(context);
        tv.setText("趣图");
        tv.setGravity(Gravity.CENTER);
        tv.setTextColor(Color.RED);
        flContent.addView(tv);
    }
}
