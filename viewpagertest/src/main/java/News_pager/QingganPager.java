package News_pager;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.viewpagertest.ListViewItem;
import com.example.viewpagertest.MyAdapter;
import com.example.viewpagertest.News_show;
import com.example.viewpagertest.R;
import com.example.viewpagertest.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 陈湘 on 2018/7/12.
 */

public class QingganPager extends NewsBase {
    private ListView lv;
    private SwipeRefreshLayout swipeRefresh;
    private List<ListViewItem> datas;
    private List<String> check_keys =new ArrayList<>();
    public QingganPager(Context context) {
        super(context);
    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            lv.setAdapter(new MyAdapter(context,datas));

        }
    };

    @Override
    public void iniData() {
        final View view_top=View.inflate(context, R.layout.list_view,null);
        //增加listView来显示新闻
        lv=(ListView)view_top.findViewById(R.id.lv);
        swipeRefresh=(SwipeRefreshLayout)view_top.findViewById(R.id.swipe_refresh);
        datas = new ArrayList<>();
        //添加轮播图的首选项
        datas.add(new ListViewItem(ListViewItem.ITEM_TYPE.TYPE_LUNBO,null,null,null,null,null,null,null,null));
        new Thread(new Runnable() {    //线程
            @Override
            public void run() {
                try {
                    URL url = new URL("http://toutiao-ali.juheapi.com/toutiao/index?type=guonei");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setConnectTimeout(5000);
                    conn.setRequestProperty("Authorization","APPCODE a66c6b2aaf6141c4b915a53df2b82ea2");

                    int code = conn.getResponseCode();
                    if (code == 200) {
                        //代表请求成功数据
                        InputStream is = conn.getInputStream();
                        String str = Util.stream2String(is);
                        JSONObject json = new JSONObject(str);
                        JSONObject result = json.getJSONObject("result");
                        JSONArray arr = result.getJSONArray("data");
                        for (int i = 0; i <arr.length() ; i++) {
                            JSONObject temp = arr.getJSONObject(i);
                            ListViewItem lv_item=new ListViewItem();
                            lv_item.setTitle(temp.optString("title"));
                            lv_item.setDate(temp.optString("date"));
                            lv_item.setAuthor(temp.optString("author_name"));
                            lv_item.setUrl(temp.optString("url"));
                            lv_item.setImg(temp.optString("thumbnail_pic_s"));
                            lv_item.setUniquekey(temp.optString("uniquekey"));
                            Log.d("pic2",temp.optString("thumbnail_pic_s02"));
                            Log.d("pic3",temp.optString("thumbnail_pic_s03"));
                            if(temp.has("thumbnail_pic_s02")&&temp.has("thumbnail_pic_s03")){
                                lv_item.setType(ListViewItem.ITEM_TYPE.TYPE_MORE_PICS);
                                lv_item.setImg2(temp.optString("thumbnail_pic_s02"));
                                lv_item.setImg3(temp.optString("thumbnail_pic_s03"));
                            }else {
                                lv_item.setType(ListViewItem.ITEM_TYPE.TYPE_ITEM);
                            }
                            check_keys.add(lv_item.getUniquekey());    //将每项Uniquekey加入集合
                            datas.add(lv_item);
                        }
                        handler.sendEmptyMessage(0);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        //每个Item的点击事件
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //DataBean bean=datas.get(position);
                ListViewItem lv_item=datas.get(position);
                Intent intent=new Intent(context,News_show.class);
                intent.putExtra("url",lv_item.getUrl());
                //startActivity(intent);
                //解决在非Activity里启动另一个Activity的办法
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {

                // adapter.notifyDataSetChanged();

                new Thread(new Runnable() {    //线程
                    @Override
                    public void run() {
                        try {
                            URL url = new URL("http://toutiao-ali.juheapi.com/toutiao/index?type=guonei");
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("GET");
                            conn.setConnectTimeout(5000);
                            conn.setRequestProperty("Authorization","APPCODE a66c6b2aaf6141c4b915a53df2b82ea2");

                            int code = conn.getResponseCode();
                            if (code == 200) {
                                //代表请求成功数据
                                InputStream is = conn.getInputStream();
                                String str = Util.stream2String(is);
                                JSONObject json = new JSONObject(str);
                                JSONObject result = json.getJSONObject("result");
                                JSONArray arr = result.getJSONArray("data");
                                for (int i = 0; i <arr.length() ; i++) {
                                    JSONObject temp = arr.getJSONObject(i);
                                    ListViewItem lv_item=new ListViewItem();
                                    //lv_item.setType(ListViewItem.ITEM_TYPE.TYPE_ITEM);
                                    lv_item.setTitle(temp.optString("title"));
                                    lv_item.setDate(temp.optString("date"));
                                    lv_item.setAuthor(temp.optString("author_name"));
                                    lv_item.setUrl(temp.optString("url"));
                                    lv_item.setImg(temp.optString("thumbnail_pic_s"));
                                    lv_item.setUniquekey(temp.optString("uniquekey"));

                                    String item_key=lv_item.getUniquekey();
                                    int j=0;
                                    for (;j<check_keys.size();j++){
                                        if(item_key.equals(check_keys.get(j))){
                                            Log.d("key_Break",item_key);
                                            break;
                                        }
                                    }
                                    if(j==check_keys.size()){
                                        Log.d("key_add",item_key);
                                        if(temp.has("thumbnail_pic_s02")&&temp.has("thumbnail_pic_s03")){
                                            lv_item.setType(ListViewItem.ITEM_TYPE.TYPE_MORE_PICS);
                                            lv_item.setImg2(temp.optString("thumbnail_pic_s02"));
                                            lv_item.setImg3(temp.optString("thumbnail_pic_s03"));
                                        }else {
                                            lv_item.setType(ListViewItem.ITEM_TYPE.TYPE_ITEM);
                                        }
                                        datas.add(1,lv_item);
                                        check_keys.add(lv_item.getUniquekey());//将每项Uniquekey加入集合
                                    }
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                swipeRefresh.setRefreshing(false); //刷新事件结束，隐藏进度条
            }
        });

        flContent.addView(view_top);
    }
}
