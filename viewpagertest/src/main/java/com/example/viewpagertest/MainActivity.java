package com.example.viewpagertest;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

import FragmentView.JokeFragment;
import FragmentView.MeFragment;
import FragmentView.NewsFragment;
import FragmentView.PicFragment;
import PagerView.BasePager;
import PagerView.JokePager;
import PagerView.MinePager;
import PagerView.NewsPager;
import PagerView.PicPager;

public class MainActivity extends AppCompatActivity {
    private NoScroolViewPager vp;
    private List<BasePager> list;
    private RadioGroup mRadioGroup;
    private RadioButton btn_1;
    private RadioButton btn_2;
    private RadioButton btn_3;
    private RadioButton btn_4;
    private RadioButton btn_5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);


        vp = (NoScroolViewPager) findViewById(R.id.vp);
        //setDate();
        //vp.setAdapter(new MyVpAdapter(list));

        mRadioGroup = (RadioGroup) findViewById(R.id.mRadiaoGroup_btn);
        btn_1 = (RadioButton) findViewById(R.id.btn_1);
        btn_2 = (RadioButton) findViewById(R.id.btn_2);
        btn_3 = (RadioButton) findViewById(R.id.btn_3);
        btn_4 = (RadioButton) findViewById(R.id.btn_4);

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.btn_1: {
                        vp.setCurrentItem(0, false);
                        break;
                    }
                    case R.id.btn_2: {
                        vp.setCurrentItem(1, false);
                        break;
                    }
                    case R.id.btn_3: {
                        vp.setCurrentItem(2, false);
                        break;
                    }
                    case R.id.btn_4: {
                        vp.setCurrentItem(3, false);
                        break;
                    }
                }
            }
        }); //点击事件
        setupViewPager(vp);
    }

    private void setDate() {
        list=new ArrayList<>();
        list.add(new NewsPager(MainActivity.this));
        list.add(new PicPager(MainActivity.this));
        list.add(new JokePager(MainActivity.this));
        list.add(new MinePager(MainActivity.this));
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(NewsFragment.newInstance("日历"));
        adapter.addFragment(PicFragment.newInstance("记事"));
        adapter.addFragment(JokeFragment.newInstance("搜索"));
        adapter.addFragment(MeFragment.newInstance("我的"));
        viewPager.setAdapter(adapter);
    }
}
