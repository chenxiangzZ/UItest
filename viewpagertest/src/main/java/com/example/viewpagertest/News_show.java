package com.example.viewpagertest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class News_show extends AppCompatActivity {
    private WebView webView;
    private Button btn_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_show);
        Intent intent=getIntent();
        String url=intent.getStringExtra("url");
        webView= (WebView)findViewById(R.id.webview);
        webView.loadUrl(url);
        btn_back=(Button)findViewById(R.id.back_button);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击返回事件的处理
                finish();
            }
        });
    }
}
