package com.example.viewpagertest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import DataBean.PictureBean;


/**
 * Created by zichaun on 2018/7/15.
 */
public class MyListAdapter extends BaseAdapter {
    private Context context;
    private List<PictureBean> lists;
    ImageView list_img;
    TextView list_intro,list_time;
    private LayoutInflater layoutInflater;

    public MyListAdapter(Context context, List<PictureBean> lists) {
        this.context = context;
        this.lists = lists;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.pic_item, null);
        }
        list_img=(ImageView) convertView.findViewById(R.id.list_img);
        list_intro=(TextView) convertView.findViewById(R.id.list_intro);
        list_time= (TextView) convertView.findViewById(R.id.list_time);
        //设置数据与控件绑定
        Glide.with(context).load(lists.get(position).getImg()).into(list_img);
        list_intro.setText(lists.get(position).getIntro());
        list_time.setText(lists.get(position).getDate());
        return convertView;
    }
}
