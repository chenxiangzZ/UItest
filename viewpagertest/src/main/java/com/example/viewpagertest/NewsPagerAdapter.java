package com.example.viewpagertest;

/**
 * Created by 陈湘 on 2018/7/12.
 */

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import News_pager.NewsBase;

public class NewsPagerAdapter extends PagerAdapter {

    private List<NewsBase>list;
    private static final String[] TITLE = new String[] { "今日热点", "娱乐", "科技", "财经",
            "军事", "体育", "情感", "旅游" };

    public NewsPagerAdapter(List<NewsBase> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        NewsBase pager = list.get(position);
        if(!pager.hasView){
            pager.iniData();
            View view = pager.mRootView;
            container.addView(view);
            pager.hasView=true;
            return view;
        }else {
            View view = pager.mRootView;
            container.addView(view);
            return view;
        }

    }

    //设置标题
    @Override
    public CharSequence getPageTitle(int position) {
        return TITLE[position % TITLE.length];
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
