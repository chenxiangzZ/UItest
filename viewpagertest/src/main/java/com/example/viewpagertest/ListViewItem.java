package com.example.viewpagertest;

/**
 * Created by 陈湘 on 2018/7/15.
 */

public class ListViewItem {

    /**
     * 类型列表
     */
    public static enum ITEM_TYPE {
        TYPE_ITEM, TYPE_LUNBO,TYPE_MORE_PICS
    };

    /**
     * 类型
     */
    private ITEM_TYPE mType;
    /**
     * 内容
     */
    private String title;
    private String date;
    private String author;
    private String url;
    private String img;
    private String uniquekey;
    private String img2;

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    private String img3;

    public ListViewItem(ITEM_TYPE tType,String title, String date, String author, String url, String img,String uniquekey ,String img2,String img3) {
        this.mType=tType;
        this.title = title;
        this.date = date;
        this.author = author;
        this.url = url;
        this.img = img;
        this.uniquekey=uniquekey;
        this.img2=img2;
        this.img3=img3;
    }
    public ListViewItem() {
    }

    public ITEM_TYPE getType() {
        return mType;
    }

    public void setType(ITEM_TYPE tType) {
        mType = tType;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUniquekey() {
        return uniquekey;
    }

    public void setUniquekey(String uniquekey) {
        this.uniquekey = uniquekey;
    }
}
