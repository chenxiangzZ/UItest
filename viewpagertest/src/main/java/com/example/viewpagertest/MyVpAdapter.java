package com.example.viewpagertest;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import PagerView.BasePager;

/**
 * Created by 18573 on 2018/7/11.
 */

public class MyVpAdapter extends PagerAdapter {
    private List<BasePager> list;

    public MyVpAdapter(List<BasePager> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        BasePager pager = list.get(position);
        if(!pager.hasView){
            pager.iniData();
            View view = pager.mRootView;
            container.addView(view);
            pager.hasView=true;   //判断是否加载了布局
            return view;
        }else {
            View view = pager.mRootView;
            container.addView(view);
            pager.hasView=true;   //判断是否加载了布局
            return view;
        }

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}

