package com.example.viewpagertest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.guoqi.actionsheet.ActionSheet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
public class DetailsActivity extends Activity implements ActionSheet.OnActionSheetSelected {

    public static final int TAKE_PHOTO=1;
    public static final int CHOOSE_PHOTO=2;

    private Uri imageUri;
    ImageView details_img;
    ImageButton details_back,select_date;
    TextView tv_birthday,living_place,hometown;
    EditText ed_name;
    EditText ed_qianming;
    int mYear,mMonth,mDay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

        details_img= (ImageView) findViewById(R.id.details_img);
        details_back= (ImageButton) findViewById(R.id.details_back);
        select_date= (ImageButton) findViewById(R.id.select_date);
        tv_birthday= (TextView) findViewById(R.id.tv_birthday);
        living_place= (TextView) findViewById(R.id.living_place);
        hometown= (TextView) findViewById(R.id.hometown);
        ed_name=(EditText)findViewById(R.id.et_name);
        ed_qianming=(EditText)findViewById(R.id.et_qianming);

//        Glide.with(DetailsActivity.this)
//                .load(R.drawable.header_img)
//                .transform(new GlideCircleTransform(DetailsActivity.this))
//                .into( details_img);

        //当点击时，把内容填充进页面
        Intent intent=getIntent();
        byte buff[] = intent.getByteArrayExtra("data_pic");
        Bitmap bitmap = BitmapFactory.decodeByteArray(buff, 0, buff.length);
        details_img.setImageBitmap(bitmap);

        ed_name.setText(intent.getStringExtra("data_name"));
        tv_birthday.setText(intent.getStringExtra("data_birthday"));

        ed_qianming.setText(intent.getStringExtra("data_qianming"));
        /*返回事件的处理，保存数据，并展现到前一个页面*/
        details_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap=setimage(details_img); //调用方法，得到返回值bitmap，Intent传递
                byte[] bytes=Bitmap2Bytes(bitmap);
                Intent intent=new Intent();
                intent.putExtra("data_pic",bytes);
                intent.putExtra("data_name",ed_name.getText().toString());
                intent.putExtra("data_qianming",ed_qianming.getText().toString());
                intent.putExtra("data_birthday",tv_birthday.getText().toString());
                setResult(RESULT_OK,intent);
                finish(); //结束并返回数据
            }
        });
         /*选择出生日期*/
        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar ca = Calendar.getInstance();
                mYear = ca.get(Calendar.YEAR);
                mMonth = ca.get(Calendar.MONTH);
                mDay = ca.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(DetailsActivity.this, onDateSetListener, mYear, mMonth, mDay).show();
            }
        });

        //图片点击的更换头像的功能
        details_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionSheet.showSheet(DetailsActivity.this, DetailsActivity.this, null);
            }
        });
    }

    @Override
    public void onClick(int whichButton) {
        switch (whichButton) {
            case ActionSheet.CHOOSE_PICTURE:
                //相册
                choosePic();
                break;
            case ActionSheet.TAKE_PICTURE:
                //拍照
                takePic();
                break;
            case ActionSheet.CANCEL:
                //取消
                break;
        }
    }

    /*日期选择器对话框监听 */
    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            monthOfYear=monthOfYear+1;
            String days=year+"年"+monthOfYear+"月"+dayOfMonth+"日";
            tv_birthday.setText(days);
        }
    };

    //加入自己的逻辑
    public void takePic(){
        //创建FIle对象，用于存储拍照后的照片
        File outputImage=new File(getExternalCacheDir(),"output_image.jpg");
        try{
            if(outputImage.exists()){
                outputImage.delete();
            }
            outputImage.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT>=24){
            imageUri= FileProvider.getUriForFile(DetailsActivity.this,"com.example.cameraalbumtest.fileprovider",outputImage);
        }else {
            imageUri=Uri.fromFile(outputImage);
        }
        //启动相机程序
        Intent intent=new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
        startActivityForResult(intent,TAKE_PHOTO);
    }

    //加入自己的逻辑
    public void choosePic(){
        if(ContextCompat.checkSelfPermission(DetailsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(DetailsActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
        }else {
            openAlbum();
        }
    }

    private void openAlbum(){
        Intent intent=new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,CHOOSE_PHOTO);//打开相册
    }

    @Override
    protected void  onActivityResult(int requestCode,int resultCode ,Intent data){
        Log.d("requestCode ", String.valueOf(requestCode));
        switch (requestCode){
            case TAKE_PHOTO:
                if (resultCode==RESULT_OK){
                    try{
                        //将拍摄的照片显示出来
                        Bitmap bitmap= BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        details_img.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CHOOSE_PHOTO:
                Log.d("resultcode1",String.valueOf(RESULT_OK));
                Log.d("resultcode2",String.valueOf(resultCode));
                if(resultCode==RESULT_OK){
                    //判断手机版本号
                    if(Build.VERSION.SDK_INT>=19){
                        Log.d("handleImageOnKitKat :","handleImageOnKitKat");
                        handleImageOnKitKat(data);
                    }else {
                        Log.d("ImageBeforeKitKat :","ImageBeforeKitKat");
                        handleImageBeforeKitKat(data);
                    }
                }
                break;
            default:
                break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permission,int []grantResults){
        switch (requestCode){
            case 1:
                if (grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    openAlbum();
                }else {
                    Toast.makeText(this,"You denied the permission",Toast.LENGTH_SHORT).show();
                }
                break;
            default:
        }
    }

    //把图片转换成bitmap形式传递通过intent形式传递过去
    private Bitmap setimage(ImageView view1){
        Bitmap image = ((BitmapDrawable)view1.getDrawable()).getBitmap();
        Bitmap bitmap1 = Bitmap.createBitmap(image);
        return bitmap1;
    }
    //bitmap转换成byte形式
    private byte[] Bitmap2Bytes(Bitmap bm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }
    @TargetApi(19)
    private void  handleImageOnKitKat(Intent data){
        String imagePath =null;
        Uri uri=data.getData();
        if(DocumentsContract.isDocumentUri(this,uri)){
            String docId= DocumentsContract.getDocumentId(uri);
            if("com.android.providers.media.documents".equals(uri.getAuthority())) {
                String id = docId.split(":")[1];
                String selection = MediaStore.Images.Media._ID + "=" + id;
                imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection);
            }else if("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                Uri contentUri = ContentUris.withAppendedId(uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                imagePath = getImagePath(contentUri, null);
            }
        } else if("content".equalsIgnoreCase(uri.getScheme())){
            imagePath=getImagePath(uri,null);
        }else if("file".equalsIgnoreCase(uri.getScheme())){
            imagePath=uri.getPath();
        }
        Log.d("imagePath: ",imagePath);
        displayImage(imagePath);
    }

    private void  handleImageBeforeKitKat(Intent data){
        Uri uri=data.getData();
        String imagePath=getImagePath(uri,null);
        Log.d("imagePath: ",imagePath);
        displayImage(imagePath);
    }

    private String getImagePath(Uri uri,String selection){
        String path =null;
        Cursor cursor=getContentResolver().query(uri,null,selection,null,null);
        if(cursor!=null){
            if(cursor.moveToFirst()){
                path=cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }
    private void displayImage(String imagePath){
        Log.d("imagePath: ",imagePath);
        if(imagePath!=null){
            Bitmap bitmap=BitmapFactory.decodeFile(imagePath);
            details_img.setImageBitmap(bitmap);
        }else {
            Toast.makeText(this,"failed to get image!",Toast.LENGTH_SHORT).show();
        }
    }
}

