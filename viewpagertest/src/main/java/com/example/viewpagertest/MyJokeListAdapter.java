package com.example.viewpagertest;

/**
 * Created by 陈湘 on 2018/7/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import DataBean.JokeBean;

/**
 * Created by zichaun on 2018/7/16.
 */
public class MyJokeListAdapter extends BaseAdapter {
    private Context context;
    private List<JokeBean> lists;
    TextView joketitle,joke_text,joke_time;
    private LayoutInflater layoutInflater;


    public MyJokeListAdapter(Context context, List<JokeBean> lists) {
        this.context = context;
        this.lists = lists;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.joke_item, null);
        }
        joketitle=(TextView) convertView.findViewById(R.id.jokeTitle);
        joke_text=(TextView)convertView.findViewById(R.id.jokeText);
        joke_time=(TextView)convertView.findViewById(R.id.jokeTime);

        //设置数据与控件绑定
        joketitle.setText(lists.get(position).getTitle());
        joke_text.setText(lists.get(position).getContent());
        joke_time.setText(lists.get(position).getUpdatetime());
        return convertView;
    }
}
