package com.example.viewpagertest;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 陈湘 on 2018/7/11.
 */

public class MyAdapter extends BaseAdapter implements OnBannerListener {
    private Context context;
    private List<ListViewItem> list;
    private Map<Integer,Boolean> map;
    private LayoutInflater mInflater;

    private Banner lv_banner;
    private ArrayList<String> list_path;
    private ArrayList<String> list_title;
    private int lunbo_count=4;

    public MyAdapter(Context context,List<ListViewItem> list) {
        this.context=context;
        mInflater=LayoutInflater.from(context);
        this.list = list;
        map = new HashMap<>();
        list_path= new ArrayList<>();
        list_title=new ArrayList<>();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView (final int position, View convertView, ViewGroup parent) {
        View view =null;
        int tType = getItemViewType(position);
        ViewHolder_Item holder_item =null;
        ViewHolder_Item_MorePic holder_item_morePic=null;
        if(tType== ListViewItem.ITEM_TYPE.TYPE_ITEM.ordinal()){
            if(convertView==null){
                view=mInflater.inflate(R.layout.news_list_item,parent,false);
                // view = View.inflate(context,R.layout.news_list_item,null);
                holder_item = new ViewHolder_Item();
                holder_item.tvName = (TextView) view.findViewById(R.id.tv_item);
                holder_item.imageView=(ImageView)view.findViewById(R.id.image_view);
                holder_item.tvAuthor=(TextView)view.findViewById(R.id.tv_author);
                holder_item.tvTime=(TextView)view.findViewById(R.id.tv_time);
                view.setTag(holder_item);

            }else {
                view = convertView;
                holder_item = (ViewHolder_Item) view.getTag();
            }
            holder_item.tvName.setText(list.get(position).getTitle());
            holder_item.tvAuthor.setText(list.get(position).getAuthor());
            holder_item.tvTime.setText(list.get(position).getDate());
            Glide.with(context).load(list.get(position).getImg()).into(holder_item.imageView);

        }else if (tType== ListViewItem.ITEM_TYPE.TYPE_LUNBO.ordinal()){
            view=mInflater.inflate(R.layout.news_list_lunbo,parent,false);
            lv_banner=(Banner)view.findViewById(R.id.lv_banner);
            //轮播操作
            //放图片地址的集合
            list_path = new ArrayList<>();
            //放标题的集合
            list_title = new ArrayList<>();

            //要采用随机的方式加载内容
            list_path.add("http://ww4.sinaimg.cn/large/006uZZy8jw1faic21363tj30ci08ct96.jpg");
            list_path.add("http://ww4.sinaimg.cn/large/006uZZy8jw1faic259ohaj30ci08c74r.jpg");
            list_path.add("http://ww4.sinaimg.cn/large/006uZZy8jw1faic2b16zuj30ci08cwf4.jpg");
            list_path.add("http://ww4.sinaimg.cn/large/006uZZy8jw1faic2e7vsaj30ci08cglz.jpg");
            list_title.add("好好学习");
            list_title.add("天天向上");
            list_title.add("热爱劳动");
            list_title.add("不搞对象");
            //设置内置样式，共有六种可以点入方法内逐一体验使用。
            lv_banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR_TITLE_INSIDE);
            //设置图片加载器，图片加载器在下方
            lv_banner.setImageLoader(new MyAdapter.MyLoader());
            //设置图片网址或地址的集合
            lv_banner.setImages(list_path);
            //设置轮播的动画效果，内含多种特效，可点入方法内查找后内逐一体验
            lv_banner.setBannerAnimation(Transformer.Default);
            //设置轮播图的标题集合
            lv_banner.setBannerTitles(list_title);
            //设置轮播间隔时间
            lv_banner.setDelayTime(3000);
            //设置是否为自动轮播，默认是“是”。
            lv_banner.isAutoPlay(true);
            //设置指示器的位置，小点点，左中右。
            lv_banner.setIndicatorGravity(BannerConfig.CENTER)
                    //以上内容都可写成链式布局，这是轮播图的监听。比较重要。方法在下面。
                    .setOnBannerListener(this)
                    //必须最后调用的方法，启动轮播图。
                    .start();
            //holder_lunbo.lv_banner=lv_banner;
        }else if(tType==ListViewItem.ITEM_TYPE.TYPE_MORE_PICS.ordinal()){
                if(convertView==null){
                    view=mInflater.inflate(R.layout.listview_morepics,parent,false);
                    // view = View.inflate(context,R.layout.news_list_item,null);
                    holder_item_morePic = new ViewHolder_Item_MorePic();
                    holder_item_morePic.tvContent=(TextView)view.findViewById(R.id.morepic_content);
                    holder_item_morePic.tvAuthor=(TextView)view.findViewById(R.id.morepic_author);
                    holder_item_morePic.tvTime=(TextView)view.findViewById(R.id.morepic_time);
                    holder_item_morePic.image1=(ImageView)view.findViewById(R.id.morepic_one);
                    holder_item_morePic.image2=(ImageView)view.findViewById(R.id.morepic_two);
                    holder_item_morePic.image3=(ImageView)view.findViewById(R.id.morepic_three);

                    view.setTag(holder_item_morePic);
                }else {
                    view = convertView;
                    holder_item_morePic = (ViewHolder_Item_MorePic)view.getTag();
                }

                holder_item_morePic.tvContent.setText(list.get(position).getTitle());
                holder_item_morePic.tvAuthor.setText(list.get(position).getAuthor());
                holder_item_morePic.tvTime.setText(list.get(position).getDate());
                Glide.with(context).load(list.get(position).getImg()).into(holder_item_morePic.image1);
                Glide.with(context).load(list.get(position).getImg2()).into(holder_item_morePic.image2);
                Glide.with(context).load(list.get(position).getImg3()).into(holder_item_morePic.image3);
            }
        return view;
    }

    /**
     * 获得某个条目的类型
     */
    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType().ordinal();
    }

    /**
     * 获得类型总数
     */
    @Override
    public int getViewTypeCount() {
        return ListViewItem.ITEM_TYPE.values().length;
    }


    public class ViewHolder_Item{
        TextView tvName;
        ImageView imageView;
        TextView tvAuthor;
        TextView tvTime;
    }

    public class ViewHolder_Item_MorePic{
        TextView tvContent;
        ImageView image1;
        ImageView image2;
        ImageView image3;
        TextView tvAuthor;
        TextView tvTime;
    }

    //轮播图的监听方法
    @Override
    public void OnBannerClick(int position) {
        Log.i("tag", "你点了第"+position+"张轮播图");
    }
    //自定义的图片加载器
    private class MyLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load((String) path).into(imageView);
        }
    }

}
