package Pic_Pager;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import com.example.viewpagertest.R;

/**
 * Created by 陈湘 on 2018/7/17.
 */

public class PicBase {
    public View mRootView;
    public Context context;
    public FrameLayout flContent;
    public Boolean hasView;
    public PicBase(Context context) {
        this.context = context;
        mRootView= initView();
        hasView=false;
    }

    /**
     * 初始化视图
     * @return
     */
    public View initView() {
        View view = View.inflate(context, R.layout.view,null);
        flContent = (FrameLayout) view.findViewById(R.id.fl_content);
        return view;
    }

    /**
     * 初始化数据
     */
    public void iniData(){

    }
}
