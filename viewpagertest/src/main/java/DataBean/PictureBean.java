package DataBean;

/**
 * Created by zichaun on 2018/7/15.
 */

public class PictureBean {
    private String img;
    private  String intro;
    private  String date;
    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public PictureBean() {
        super();
    }
    public PictureBean(String intro, String img) {
        this.intro = intro;
        this.img = img;

    }
}
