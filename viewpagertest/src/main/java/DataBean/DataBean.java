package DataBean;

/**
 * Created by 陈湘 on 2018/7/13.
 */

public class DataBean {
    private String title;
    private String date;
    private String author;
    private String url;
    private String img;

    public DataBean(String title, String date, String author, String url, String img) {
        this.title = title;
        this.date = date;
        this.author = author;
        this.url = url;
        this.img = img;
    }

    public DataBean() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
