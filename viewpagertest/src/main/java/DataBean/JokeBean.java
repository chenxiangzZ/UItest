package DataBean;

/**
 * Created by zichaun on 2018/7/16.
 */

public class JokeBean {
    private  String content;
    private  String title;
    private  String updatetime;
    private  String hashid;


    public JokeBean(String content, String title, String updatetime, String hashid) {
        this.content = content;
        this.title = title;
        this.updatetime = updatetime;
        this.hashid = hashid;
    }

    public JokeBean() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getHashid() {
        return hashid;
    }

    public void setHashid(String hashid) {
        this.hashid = hashid;
    }
}
