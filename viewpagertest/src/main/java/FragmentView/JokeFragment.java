package FragmentView;

/**
 * Created by 陈湘 on 2018/7/16.
 */

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.viewpagertest.MyJokeListAdapter;
import com.example.viewpagertest.R;
import com.example.viewpagertest.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import DataBean.JokeBean;


/**
 * Created by 陈湘 on 2018/6/24.
 */

public class JokeFragment extends Fragment {
    private ListView listView;
    private List<JokeBean> list;
    public static JokeFragment newInstance(String info) {
        Bundle args = new Bundle();
        JokeFragment fragment = new JokeFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //lv.setAdapter(new MyAdapter(MainActivity.this,datas));
            listView.setAdapter(new MyJokeListAdapter(getContext(),list));
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.joke, null);
        listView= (ListView) view.findViewById(R.id.jokeview_item);
        list = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //  String addr="http://route.showapi.com/341-1?showapi_appid%3D100%26page%3D1%26maxResult%3D20%26showapi_sign%3D698d51a19d8a121ce581499d7b701668";
                    String addr = "http://route.showapi.com/341-1?showapi_appid=69877&showapi_sign=49b4d5a9860c4fb08098bf240f8d91dc";
                    //String addr="http://v.juhe.cn/joke/content/list.php?key=65ffc9e8495679f5f14dcc0351edce51&pagesize=10&sort=desc&time=1531729862";
                    URL url = new URL(addr);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setConnectTimeout(10000);
                    //conn.setRequestProperty("Authorization","APPCODE 65ffc9e8495679f5f14dcc0351edce51");
                    int code = conn.getResponseCode();
                    if (code == 200) {
                        //代表请求成功数据
                        InputStream is = conn.getInputStream();
                        String str = Util.stream2String(is);
                        JSONObject json = new JSONObject(str);
                        /*JSONObject result = json.getJSONObject("result");
                        JSONArray arr = result.getJSONArray("data");*/
                        JSONObject result = json.getJSONObject("showapi_res_body");
                        JSONArray arr = result.getJSONArray("contentlist");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject temp = arr.getJSONObject(i);
                            JokeBean bean = new JokeBean();
                            bean.setTitle(temp.optString("title"));
                            bean.setContent(temp.optString("text"));
                            bean.setUpdatetime(temp.optString("ct"));
                            list.add(bean);

                        }
                        handler.sendEmptyMessage(0);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return view;
    }

}
