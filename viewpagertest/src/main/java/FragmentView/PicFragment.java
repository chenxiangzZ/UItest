package FragmentView;

/**
 * Created by 陈湘 on 2018/7/16.
 */

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.viewpagertest.MyListAdapter;
import com.example.viewpagertest.R;
import com.example.viewpagertest.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import DataBean.PictureBean;


public class PicFragment extends Fragment {
    public ListView listView;
    private List<PictureBean>list;
    public static PicFragment newInstance(String info) {
        Bundle args = new Bundle();
        PicFragment fragment = new PicFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            listView.setAdapter(new MyListAdapter(getContext(),list));
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.joke, null);
        //增加listView来显示新闻
        //lv=(ListView)view_top.findViewById(R.id.lv);
        listView= (ListView) view.findViewById(R.id.jokeview_item);
        list = new ArrayList<PictureBean>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://route.showapi.com/341-2?showapi_appid=69877&showapi_sign=49b4d5a9860c4fb08098bf240f8d91dc");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setConnectTimeout(10000);
                    // conn.setRequestProperty("Authorization","APPCODE 0a80f821f5e14081b8177422ac23b93c");
                    int code = conn.getResponseCode();
                    if (code == 200) {
                        //代表请求成功数据
                        InputStream is = conn.getInputStream();
                        String str = Util.stream2String(is);
                        JSONObject json = new JSONObject(str);
                        JSONObject result = json.getJSONObject("showapi_res_body");
                        JSONArray arr = result.getJSONArray("contentlist");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject temp = arr.getJSONObject(i);
                            PictureBean bean = new PictureBean();
                            bean.setIntro(temp.optString("title"));
                            bean.setImg(temp.optString("img"));
                            bean.setDate(temp.optString("ct"));
                            list.add(bean);

                        }
                        handler.sendEmptyMessage(0);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return view;
    }
}
