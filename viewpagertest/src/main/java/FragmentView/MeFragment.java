package FragmentView;

/**
 * Created by 陈湘 on 2018/7/16.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.viewpagertest.DetailsActivity;
import com.example.viewpagertest.Login;
import com.example.viewpagertest.R;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;
import static com.example.viewpagertest.R.layout.me;

/**
 * Created by 陈湘 on 2018/7/16.
 */


/**
 * Created by 陈湘 on 2018/6/24.
 */

public class MeFragment extends Fragment {
    private ImageView me_iv;
    TextView tv_name,tv_qianming;
    private String name ="杨湘延博";
    private String birthday="1997年1月1日";
    private String qianming="Stay Hungry!";

    private RelativeLayout rl_details;
    private RelativeLayout rl_version;
    private RelativeLayout rl_developer;
    private RelativeLayout rl_setting;

    public static MeFragment newInstance(String info) {
        Bundle args = new Bundle();
        MeFragment fragment = new MeFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(me, null);

        me_iv= (ImageView) view.findViewById(R.id.me_iv);
        tv_name=(TextView)view.findViewById(R.id.tv_name);
        tv_qianming=(TextView)view.findViewById(R.id.tv_qianming);
        rl_details=(RelativeLayout) view.findViewById(R.id.rl_details);
        rl_version=(RelativeLayout) view.findViewById(R.id.rl_version);
        rl_developer=(RelativeLayout) view.findViewById(R.id.rl_developer);
        rl_setting=(RelativeLayout) view.findViewById(R.id.rl_setting);
        me_iv.setImageResource(R.drawable.header_img);
//Glide加载会导致imageView.getDrawable() 获取的Drawable 类型不是BitmapDrawable
//        而是 GlideBitmapDrawable
//        因此类型错误导致
//            Glide.with(getContext())
//                    .load(R.drawable.header_img)
//                    .transform(new GlideCircleTransform(getContext()))
//                    .into(me_iv);
        rl_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailsDialog();
            }
        });
        rl_developer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeveloperDialog();
            }
        });
        rl_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VersonDialog();
            }
        });
        rl_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(),Login.class);
                startActivityForResult(intent,1);
            }
        });

        return view;
    }

    private void DetailsDialog() {
        Intent intent=new Intent(getContext(), DetailsActivity.class);
        Bitmap bitmap=setimage(me_iv); //调用方法，得到返回值bitmap，Intent传递
        byte[] bytes=Bitmap2Bytes(bitmap);
        intent.putExtra("data_pic",bytes);

        intent.putExtra("data_name",name);
        intent.putExtra("data_qianming",qianming);
        intent.putExtra("data_birthday",birthday);

        startActivityForResult(intent,1);

        //在mainActivity里写Onresult函数
    }

    //在这里写有关forresult的方法，在其他非Activity的地方不能写。
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    byte buff[] = data.getByteArrayExtra("data_pic");
                    Bitmap bitmap = BitmapFactory.decodeByteArray(buff, 0, buff.length);
                    ImageView imageView = (ImageView) getActivity().findViewById(R.id.me_iv);
                    imageView.setImageBitmap(bitmap);

                    name=data.getStringExtra("data_name");
                    birthday=data.getStringExtra("data_birthday");
                    int year=2018-Integer.valueOf(birthday.substring(0,4));
                    tv_name.setText(name+" | "+ String.valueOf(year)+"岁");

                    qianming=data.getStringExtra("data_qianming");
                    tv_qianming.setText(qianming);
                }
                break;
        }
    }

    private void VersonDialog() {
        TextView msg = new TextView(getContext());
        msg.setText("\n"+"V1.0\n" +"Copyright © 2018 Group4\n" +  "All Right Reserved\n");
        msg.setPadding(30, 10, 10, 5);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(18);
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle("版本号：")
                .setView(msg)
                .setPositiveButton("确认",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                } )
                .create();
        dialog.show();
    }

    private void DeveloperDialog() {
        TextView msg = new TextView(getContext());
        msg.setText("\n"+"杨铮伟\n" +"陈湘\n" +  "侯忠博\n" +"黄延\n");
        msg.setPadding(30, 10, 10, 5);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(18);
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle("Designed By: Group4")
                .setView(msg)
                .setPositiveButton("确认",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                } )
                .create();
        dialog.show();
    }

    //把图片转换成bitmap形式传递通过intent形式传递过去
    private Bitmap setimage(ImageView view1){
        Bitmap image = ((BitmapDrawable)view1.getDrawable()).getBitmap();
        Bitmap bitmap1 = Bitmap.createBitmap(image);
        return bitmap1;
    }
    //bitmap转换成byte形式
    private byte[] Bitmap2Bytes(Bitmap bm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }
}
