package FragmentView;

/**
 * Created by 陈湘 on 2018/7/16.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.viewpagertest.NewsPagerAdapter;
import com.example.viewpagertest.R;
import com.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

import News_pager.CaijinPager;
import News_pager.JunshiPager;
import News_pager.KejiPager;
import News_pager.LvyouPager;
import News_pager.NewsBase;
import News_pager.QingganPager;
import News_pager.TiyuPager;
import News_pager.ToutiaoPager;
import News_pager.YulePager;


/**
 * Created by 陈湘 on 2018/6/24.
 */

public class NewsFragment extends Fragment {

    private List<NewsBase> list;

    private SearchView sv_search;
    public static NewsFragment newInstance(String info) {
        Bundle args = new Bundle();
       NewsFragment fragment = new NewsFragment();
        args.putString("info", info);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view_top = inflater.inflate(R.layout.news_top, null);
        //增加listView来显示新闻
        //lv=(ListView)view_top.findViewById(R.id.lv);
        sv_search=(SearchView)view_top.findViewById(R.id.sv) ;
        TabPageIndicator indicator=(TabPageIndicator) view_top.findViewById(R.id.indicator);
        ViewPager pager=(ViewPager)view_top.findViewById(R.id.pager);
        //searchview的设置
        setSearchView(sv_search);

        setDate();
        NewsPagerAdapter adapter=new NewsPagerAdapter(list);
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        //flContent.addView(view_top);



        //banner = (Banner) view_top.findViewById(R.id.banner);
        return view_top;
    }

    private void setSearchView(SearchView sv_search) {
        // 设置SearchView默认是否自动缩小为图标
        sv_search.setFocusable(false);
        sv_search.setIconifiedByDefault(false);
        // 显示搜索按钮
        sv_search.setSubmitButtonEnabled(true);
        SpannableString spanText = new SpannableString("搜你想搜的");
        // 设置字体大小
        spanText.setSpan(new AbsoluteSizeSpan(15, true), 0, spanText.length(),
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        // 设置字体颜色
        spanText.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                spanText.length(),
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        sv_search.setQueryHint(spanText);

        //sv_search.setBackgroundColor(sv_search.getContext().getResources().getColor(R.color.white));
        int id = sv_search.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) sv_search.findViewById(id);
        textView.setTextSize(15);// 设置输入字体大小
        textView.setTextColor(Color.BLACK);// 设置输入字的显示
        textView.setHeight(50);// 设置输入框的高度
        textView.setGravity(Gravity.CENTER_VERTICAL);
    }

    private void setDate() {
        //这里的顺序对应内容的顺序
        list=new ArrayList<>();
        list.add(new ToutiaoPager(getContext()));
        list.add(new YulePager(getContext()));
        list.add(new KejiPager(getContext()));
        list.add(new CaijinPager(getContext()));
        list.add(new JunshiPager(getContext()));
        list.add(new TiyuPager(getContext()));
        list.add(new QingganPager(getContext()));
        list.add(new LvyouPager(getContext()));

    }
}

