package Tools;

/**
 * Created by 陈湘 on 2018/7/11.
 */

public class DataBean {
    private String str;
    private int imageId;
    public DataBean(String str,int imageId) {
        this.imageId=imageId;
        this.str=str;
    }

    public String getStr(){
        return str;
    }

    public int getImageId(){
        return imageId;
    }
}
