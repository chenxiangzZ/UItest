package Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.listviewtest.R;

import java.util.List;

/**
 * Created by 陈湘 on 2018/7/11.
 */

public class MyAdapter extends ArrayAdapter<DataBean> {
    private int resourceId;
    public MyAdapter(Context context,int textViewResourceId,List<DataBean> objects) {
        super(context,textViewResourceId,objects);
        resourceId=textViewResourceId;
    }


    @Override
    public int getCount() {
        //return list.size();
        return 0;
    }

    @Override
    public DataBean getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataBean bean= getItem(position);
        View view= LayoutInflater.from(getContext()).inflate(resourceId,parent,false);
        //View view=View.inflate(context,R.layout.item,null);
        ImageView img=(ImageView)view.findViewById(R.id.img);
        TextView tvName=(TextView)view.findViewById(R.id.tv_item);
        img.setImageResource(bean.getImageId());
        tvName.setText(bean.getStr());
        return view;
    }
}
