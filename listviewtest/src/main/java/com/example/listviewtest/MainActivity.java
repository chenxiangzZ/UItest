package com.example.listviewtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

import Tools.Adapter;
import Tools.DataBean;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private ArrayList<DataBean> list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setDate();

        listView=(ListView)findViewById(R.id.list_view);

        listView.setAdapter(new Adapter(MainActivity.this,R.layout.item,list));
    }
    private void setDate() {
        for(int i=0;i<100;i++){
            DataBean bean=new DataBean("新闻"+i,R.mipmap.xinxin);
            list.add(bean);
        }
    }
}
