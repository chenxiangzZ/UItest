package com.example.uitest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private AlertDialog alertDialog;
    private TextView tx;
    private Button btn_1;
    private Button btn_2;
    private Button btn_3;
    private Button btn_4;
    private Button btn_5;
    private Button btn_6;
    private Button btn_7;
    private Button btn_8;
    private Button btn_9;
    private Button btn_0;
    private Button btn_11;
    private Button btn_12;
    private ImageButton btn_chexiao;
    private ImageButton btn_boda;
    private Button btn_bohao;
    private Button btn_jilu;
    private Button btn_tongxunlu;
    private Button btn_shoucang;
    private String str="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tx=(TextView)findViewById(R.id.textView);
        btn_1=(Button)findViewById(R.id.btn_1);
        btn_2=(Button)findViewById(R.id.btn_2);
        btn_3=(Button)findViewById(R.id.btn_3);
        btn_4=(Button)findViewById(R.id.btn_4);
        btn_5=(Button)findViewById(R.id.btn_5);
        btn_6=(Button)findViewById(R.id.btn_6);
        btn_7=(Button)findViewById(R.id.btn_7);
        btn_8=(Button)findViewById(R.id.btn_8);
        btn_9=(Button)findViewById(R.id.btn_9);
        btn_0=(Button)findViewById(R.id.btn_10);
        btn_11=(Button)findViewById(R.id.btn_11);
        btn_12=(Button)findViewById(R.id.btn_12);
        btn_chexiao=(ImageButton)findViewById(R.id.btn_chexiao);
        btn_boda=(ImageButton)findViewById(R.id.btn_boda);
        btn_bohao=(Button)findViewById(R.id.btn_bohao);
        btn_shoucang=(Button)findViewById(R.id.btn_shoucang);
        btn_tongxunlu=(Button)findViewById(R.id.btn_tongxunlu);
        btn_jilu=(Button)findViewById(R.id.btn_jilu);
        //默认初始拨号被点击
        btn_bohao.setEnabled(false);

        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="1";
                }
                tx.setText(str);
            }
        });
        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="2";
                }
                tx.setText(str);
            }
        });
        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="3";
                }
                tx.setText(str);
            }
        });
        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="4";
                }
                tx.setText(str);
            }
        });
        btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="5";
                }
                tx.setText(str);
            }
        });
        btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="6";
                }
                tx.setText(str);
            }
        });
        btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="7";
                }
                tx.setText(str);
            }
        });
        btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="8";
                }
                tx.setText(str);
            }
        });
        btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="9";
                }
                tx.setText(str);
            }
        });
        btn_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="0";
                }
                tx.setText(str);
            }
        });
        btn_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="*";
                }
                tx.setText(str);
            }
        });
        btn_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()<15){
                    str+="#";
                }
                tx.setText(str);
            }
        });
        btn_chexiao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str.length()>0){
                    str=str.substring(0,str.length()-1);
                    Log.d("length",String.valueOf(str.length()));
                    tx.setText(str);
                }
            }
        });
        btn_boda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder  = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("打电话" ) ;
                builder.setMessage(str) ;
                builder.setPositiveButton("返回", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                builder.setNegativeButton("拨打", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                     //拨打电话的处理
                        Toast.makeText(getApplication(),str,Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog= builder.show();
            }
        });
        btn_bohao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_bohao.setEnabled(false);
                btn_tongxunlu.setEnabled(true);
                btn_shoucang.setEnabled(true);
                btn_jilu.setEnabled(true);
            }
        });
        btn_tongxunlu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_bohao.setEnabled(true);
                btn_tongxunlu.setEnabled(false);
                btn_shoucang.setEnabled(true);
                btn_jilu.setEnabled(true);
            }
        });
        btn_shoucang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_bohao.setEnabled(true);
                btn_tongxunlu.setEnabled(true);
                btn_shoucang.setEnabled(false);
                btn_jilu.setEnabled(true);
            }
        });
        btn_jilu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_bohao.setEnabled(true);
                btn_tongxunlu.setEnabled(true);
                btn_shoucang.setEnabled(true);
                btn_jilu.setEnabled(false);
            }
        });
    }
}
